
.. raw:: html

   <a rel="me" href="https://qoto.org/@grenobleluttes"></a>


.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥
.. 🤪
.. ⚖️ 👨‍🎓
.. 🌍 ♀️
.. 🇮🇷
.. 🎥 🎦

.. https://framapiaf.org/web/tags/xpipe.rss


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/links/rss.xml>`_


.. un·e

.. _grenoble_logiciel_libre:

=================================================
**Logiciel libre dans la région grenobloise**
=================================================

- https://fr.wikipedia.org/wiki/Free_Software_Foundation#Le_logiciel_libre
- :ref:`grenoble_infos:agendas_grenoble`


.. toctree::
   :maxdepth: 3

   associations/associations
   alposs/alposs
   news/news

