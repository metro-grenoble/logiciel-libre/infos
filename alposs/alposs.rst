.. index::
   ! AlpOSS

.. _alposs:

=================================================
AlpOSS
=================================================

- https://colter.social/@alposs
- https://colter.social/@nicolasvivant
- https://pouet.chapril.org/@arawa

Accès

👉 Lieu : Mairie d’Échirolles
📍Adresse : 1 place des 5 fontaines, 38130 Échirolles


Situé dans la banlieue de Grenoble, l’hôtel de ville d’Échirolles est accessible
en train, en bus, en tramway et en voiture (stationnement gratuit).


Années
========

.. toctree::
   :maxdepth: 3

   2024/2024
