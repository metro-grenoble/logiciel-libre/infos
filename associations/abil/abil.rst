.. index::
   pair: Association; ABIL (L'Atelier de Bidouille Informatique Libre de Grenoble)
   ! ABIL (L'Atelier de Bidouille Informatique Libre de Grenoble)


.. _abil:

========================================================================
**ABIL** (L'Atelier de Bidouille Informatique Libre de Grenoble)
========================================================================

- http://www.abil-grenoble.org/
- https://www.le-tamis.info/structure/atelier-de-bidouille-informatique-libre-de-grenobl

Informations
============

Places limitées, `inscription obligatoire <https://questionnaire.gresille.org/index.php/956377>`_

Vous souhaitez mais vous n'arrivez pas tout seul à  :

- résoudre un problème de logiciel;
- trouver et installer un logiciel
- utiliser et/ou apprendre à  utiliser un logiciel;
- réinstaller ou installer un système d'exploitation;
- monter un ordinateur;
- réparer un ordinateur;
- créer et/ou mettre à  jour un site oueb;
- ou d'autres choses sur un ordinateur.

Le principe de l'atelier
==================================

Cet atelier devrait vous permettre de réaliser vous même votre projet,
mais en compagnie d'autres personnes.

Ni experts de l'informatique, ni réparateurs·rices·s, ils·les·s ont
l'habitude de la bidouille et seront là  pour vous aider à  trouver
l'information là  où elle se trouve (si cela existe).

Pendant les ateliers, nous mettons du matériel à  votre disposition
(postes de travail, unités centrales à  remonter ou installer, écrans,
claviers, souris, pièces détachées, connexion Internet ...).


Quand et où ?
=================

Tous les lundis (sauf vacances scolaires), de 19h00 à  21h00. au Centre
de Loisirs Enfance et Famille (CLEF), à  Grenoble (2, rue Henri Ding - 1er étage,
mais accessible aux personnes handicapées).

Places limitées, `inscription obligatoire <https://questionnaire.gresille.org/index.php/956377>`_


- https://www.openstreetmap.org/#map=18/45.18335/5.72703

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
       src="https://www.openstreetmap.org/export/embed.html?bbox=5.723485350608827%2C45.181802405502694%2C5.730566382408142%2C45.184899092323384&amp;layer=mapnik"
       style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=18/45.18335/5.72703">Afficher une carte plus grande</a></small>



Préparer votre venue
========================

Si vous venez pour une (ré-)installation de votre système, cela peut
prendre un peu de temps. Aussi, venez avant 19h.

Si vous arrivez après, on pourra juste prendre le temps d'en discuter
avec vous et planifier à§a pour une prochaine fois.

Toujours dans le cas d'une (ré-)installation, sauvegardez vos données
sur un autre support avant de venir si vous le pouvez. Cela nous fera
gagner beaucoup de temps potentiellement.

Install Party
=====================

Votre PC devient lent ? envie d'émancipation ? ras le bol de Windows ?

Tous les premiers lundis du mois, une install party, un atelier dédié à
l'installation de distribution Linux sur votre machine.

Si vous êtes intéressés, merci de vous manifester par courriel
( "contact" suivi de "@abil-grenoble.org" ) et surtout, ne venez pas
trop tard, (avant 19h) car selon les matériels, une installation peut
prendre du temps.

A bientôt pour une install party ?





