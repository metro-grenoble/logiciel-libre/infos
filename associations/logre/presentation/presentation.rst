
.. _logre_presentation:

===============
Présentation
===============

Les statuts
===============

Article 1 – Dénomination
----------------------------

Il est fondé entre les adhérents aux présents statuts une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour titre Laboratoire Ouvert Grenoblois.


Article 2 – Objet
-----------------------

Cette association a pour but :

- d’offrir à ses adhérents un espace de travail et des ressources communes,
  destinés à la réalisation de projets coopératifs ayant une composante
  scientifique ou technique ;
- de favoriser la transmission non marchande des savoir-faire et connaissances
  scientifiques ou techniques, notamment en publiant le travail des adhérents
  sur les projets effectués dans le cadre de l’association ;
- d’engager des actions susceptibles d’accroître la liberté d’utiliser,
  de créer, d’analyser, de modifier les objets technologiques ;
- d’agir pour la promotion des sciences et techniques auprès du grand public
  en organisant et participant à des évènements.

Article 3 – Siège social
-------------------------------

Le siège social est fixé à Grenoble.
