.. index::
   pair: Association; Logre
   ! Logre (L.aboratoire O.uvert G.renoblois)

.. _logre:

=========================================================================================================================
**Logre** (L.aboratoire O.uvert G.renoblois)
=========================================================================================================================

- https://www.logre.eu/

.. toctree::
   :maxdepth: 3

   presentation/presentation
   evenements/evenements
