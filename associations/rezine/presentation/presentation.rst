
.. _presentation:

===============
Présentation
===============

Rézine est une association à but non lucratif qui fournit de l'accès à
internet, organise des ateliers et discussions.

Par son action, Rézine milite et agit pour garantir la liberté d'expression
et la communication libre entre internautes.

L'association est centrée sur la région grenobloise et ses membres se sont
donné pour objectif de fournir des accès à Internet, sans contrôle ni filtrage.

Plutôt que de confier ses échanges et ses données à de lointain·e·s inconnu·e·s
intermédiaires commerciaux, Rézine œuvre à la constitution d'un Internet
acentré et controlé par ses utilisateurs.
