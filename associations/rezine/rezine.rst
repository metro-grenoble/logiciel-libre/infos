.. index::
   pair: Association; Rézine
   ! Rézine (Accès Internet non lucratif Autour de la région grenobloise Ouvert, neutre et controlé par ses usagers)

.. _rezine:

=========================================================================================================================
**Rézine** (Accès Internet non lucratif Autour de la région grenobloise Ouvert, neutre et controlé par ses usagers)
=========================================================================================================================

- https://www.rezine.org/

.. toctree::
   :maxdepth: 3

   presentation/presentation
   ressources/ressources
   glossaire/glossaire
