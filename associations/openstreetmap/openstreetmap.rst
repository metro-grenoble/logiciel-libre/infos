.. index::
   pair: Association; Openstreetmap
   ! Openstreetmap

.. _openstreetmap:
.. _osm:

=========================================================================================================================
**Openstreetmap** (Groupe local Grenoble)
=========================================================================================================================

- :ref:`osm:openstreetmap`
- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local
- http://listes.openstreetmap.fr/wws/info/local-grenoble
- https://piaille.fr/@OsmGrenoble, @OsmGrenoble@piaille.fr
- https://forum.openstreetmap.fr/
- https://forum.openstreetmap.fr/c/groupes-locaux/grenoble/56
- http://www.agendadulibre.org/tags/openstreetmap
- https://www.echosciences-grenoble.fr/evenements
- https://turbine.coop/programmation/
- https://wiki.openstreetmap.org/wiki/Telegram
- https://web.telegram.im/#@osmfr
- http://listes.openstreetmap.fr/wws/arc/local-grenoble
- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local/Liens_pour_d%C3%A9mos


.. toctree::
   :maxdepth: 3


   agenda/agenda
   presentation/presentation
   actions/actions
   membres/membres
   themes/themes
