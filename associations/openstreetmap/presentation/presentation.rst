

.. _openstreetmap_presentation:

=========================================================================================================================
Présentation
=========================================================================================================================



Nous contacter

Le groupe local dispose de plusieurs moyens de communication :

- l'adresse de contact contact-grenoble@listes.openstreetmap.fr, moyen privilégié pour nous contacter.
- la liste de discussions `local-grenoble <http://listes.openstreetmap.fr/wws/info/local-grenoble>`_,
  à laquelle vous êtes encouragés à vous abonner
- nos comptes Twitter @OsmGrenoble (nitter) et Mastodon @OsmGrenoble@piaille.fr pour les annonces principales.
- le forum `OpenStreetMapFrance <https://forum.openstreetmap.fr/>`_ où il existe depuis le 5 décembre 2021 une
  catégorie Grenoble et une section du groupe local.

- ce `wiki <https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local>`_ (page Grenoble groupe local et sous-pages).
- les annonces principales sont reprises sur `l'Agenda du Libre <http://www.agendadulibre.org/tags/openstreetmap>`_  (avec le tag #openstreetmap).
- La Turbine Coop relaie nos annonces sur le programme de la Turbine et
  parfois sur `EchoSciences Grenoble <https://www.echosciences-grenoble.fr/evenements>`_.

Retrouvez également au niveau France, le canal de discussion Telegram qui est très actif.
