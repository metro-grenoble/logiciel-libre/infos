.. index::
   pair: Atelier contribution sur mobile; 2022-06-13


.. _openstreet_map_2022-06_13:

=========================================================================================================================
2022-06-13 19h00 **Atelier contribution sur mobile** à la Turbine
=========================================================================================================================

- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local/Archives/2021-2022#Lundi_13_juin_2022_:_Atelier_contribution_sur_mobile
- https://github.com/westnordost/StreetComplete
- https://github.com/DoFabien/OsmGo

Compte-Rendu par Binnette
==============================

`Binnette <https://wiki.openstreetmap.org/wiki/User:Binnette>`_

OsmAnd
-------

Nous avons BEAUCOUP parlé d'OsmAnd. Étant donné que je suis un aficionados
d'OsmAnd, j'ai tenté de présenter mes fonctionnalités préférées parmi les
nombreuses proposées par OsmAnd : version OpenSource/gratuite/payante,
téléchargement des cartes, ombrage du relief, courbes de niveaux, navigation,
recherche de POI, prise de notes (audio/photo/vidéo), contribution à OSM,
planification de parcours, affichage d'une imagerie satellite, affichage
du trafic routier avec l'API Google, import de traces GPX, import de favoris
au format GPX.

uMap
------


Puis on a un peu dérivé sur uMap afin par exemple de créer des fichier
de favoris depuis/vers uMap, etc.

Osm Go!
----------

Nous avons enchainé sur Osm Go! présenté par escribis.

Téléchargement de l'appli, contribution, les défauts, les points d'amélioration,
la version 1.6.0 à venir, etc.

StreetComplete
--------------

Un point rapide sur StreetComplete. Présentation de l'appli, du concept
de quête, etc.

MapComplete
-------------

En fin de session, j'ai tenu à présenté MapComplete.

Certes, c'est un "site web" mais il est parfaitement utilisable sur téléphone.

Le design de l'appli est adapté, on peut également utiliser la position
GPS du téléphone et prendre des photos.

Le gros avantage de MapComplete c'est que l'on peut créer un "thème" ou
utiliser un thème prédéfini.

Nous avons testé le thème "banc" qui, comme son nom l'indique, affiche
sur la carte tous les bancs des alentours, avec la possibilité de modifier
le banc en répondant simplement à des questions (ou en ajoutant une photo):
"Ce banc dispose-t-il d'un dossier ?", "De combien de places dispose ce banc ?",
"Quel est le matériau de ce banc", etc.

Le gros avantage de cette appli est de pouvoir faire de la cartographie
de terrain spécialisée par centre d’intérêt.

NB : on peut également créer soi-même son thème personnalisé, pas besoin
de connaître la programmation, par contre il faudra être à l'aise avec
le format JSON et lire la documentation afin de constituer son propre thème.


Programme
============

19h à la Turbine — Tout public

Première partie à la Turbine
----------------------------------

Nous vous présenterons des applications mobiles permettant de charger les
cartes OSM sur votre mobile pour les consulter sans connexion de données
(applis OSMAnd et Maps.me)

Nous vous initierons ensuite à la contribution  :

- en prenant des photos géolocalisées avec son smartphone.
- directement sur mobile Android :

    - StreetComplete Playstore f-droid Site wiki Contribution dirigée ("quêtes") ne nécessite aucune connaissance d'OSM

      - https://play.google.com/store/apps/details?id=de.westnordost.streetcomplete
      - https://f-droid.org/repository/browse/?fdid=de.westnordost.streetcomplete
      - https://github.com/westnordost/StreetComplete
      - https://wiki.openstreetmap.org/wiki/FR:StreetComplete

    - OSM Go! Playstore Site DocAjout de POI. Très efficace pour
      l'ajout/modification de commerces (utilisable sans connaître les tags OSM)

      - https://play.google.com/store/apps/details?id=fr.dogeo.osmgo
      - https://github.com/DoFabien/OsmGo
      - https://github.com/DoFabien/OsmGo/wiki

    - Every door site un nouvel éditeur de POI

      - https://every-door.app/

- en enregistrant des traces GPS permettant une contribution différée (en conjonction avec la prise de photos géolocalisées)

    GPSLogger f-droid site Un traceur GPS léger, économe en énergie.
    myTracks (iOS) Site utilise les cartes OSM.
    OpenGpxTracker (iOS) Site Wiki Logiciel libre, utilise les cartes OSM.


Deuxième partie : déambulation dans le quartier
---------------------------------------------------


Vous contribuerez « en situation » sur vos téléphones mobiles Android et iOS.


Troisième partie à la Turbine
----------------------------------

Des contributeurs expérimentés vous montreront comment analyser les
modifications effectuées par les applications et reporter les informations
collectées sur le terrain.

- Vérification des modifications effectuées automatiquement par les
  applications Android.
- Utilisation des traces GPX dans JOSM (avec le calage des photos
  géolocalisées sur la trace GPX) comme aide à la contribution.


