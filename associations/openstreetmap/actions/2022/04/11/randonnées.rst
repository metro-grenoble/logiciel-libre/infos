.. index::
   pair: Openstreetmap; 2022-04-11
   ! Randonnées

=========================================================================================================================
2022-04-11 **Randonnées**
=========================================================================================================================

- https://raw.githubusercontent.com/NicolasGrosjean/OSM_hiking_prez_2022/main/exports/slides.pdf
- https://hiking.waymarkedtrails.org/#?map=13.0/45.1853/5.7595
- https://wiki.openstreetmap.org/wiki/Guide_pour_cartographier_des_chemins_de_randonn%C3%A9es
- https://github.com/NicolasGrosjean/OSM_GAM_Hiking


Présentation des outils utilisant OSM pour la randonnée et comment
contribuer à enrichir les réseaux de carrefours dans OSM par :ref:`Grosjen <grosjean>` et Binnette.

- `Slides de la présentation <https://github.com/NicolasGrosjean/OSM_hiking_prez_2022/raw/main/exports/slides.pdf>`_
- `GitHub de la présentation <https://github.com/NicolasGrosjean/OSM_hiking_prez_2022>`_
- `Guide pour cartographier des chemins de randonnées <https://wiki.openstreetmap.org/wiki/Guide_pour_cartographier_des_chemins_de_randonn%C3%A9es>`_
- `GitHub des premiers essais de conflation avec l'OpenData de la Métro <https://github.com/NicolasGrosjean/OSM_GAM_Hiking>`_


