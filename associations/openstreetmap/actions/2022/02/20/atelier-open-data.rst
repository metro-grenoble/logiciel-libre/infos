
.. index::
   pair: OSM ; Atelier open data (2023-02-20)
   pair: OSM; OSM_Conflator


.. _osm_2023_02_20:

=========================================================================================================================
2023-02-20 19h00 **Atelier open data**
=========================================================================================================================

- https://github.com/JVillafruela/atelier-osm-conflator/blob/master/presentation-conflation/atelier-2022-02-21.md
- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local/Agenda#Lundi_20_f%C3%A9vrier_Atelier_Open_Data
- https://wiki.openstreetmap.org/wiki/OSM_Conflator
- https://github.com/mapsme/osm_conflate

Public visé : contributeurs et contributrices.

L'atelier portera sur l'utilisation des données Open Data pour enrichir OpenStreetMap.

- Exposé des règles et bonnes pratiques gouvernant les imports de données
- Étude de cas : importation des arbres de Grenoble (par :ref:`Binnette <binnette>`)
- Présentation de l'outil OSM Conflator (par :ref:`colargol <colargol>`)

  - source de la présentation
  - Script python de conflation
