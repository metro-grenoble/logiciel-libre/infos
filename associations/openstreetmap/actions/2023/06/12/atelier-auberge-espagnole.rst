.. index::
   pair: OSM ; Atelier auberge espagnole (2023-06-12)

.. osm_2023_06_12:

=========================================================================================================================
2023-06-12 19h00 **Atelier auberge espagnole**
=========================================================================================================================

- https://wiki.openstreetmap.org/wiki/Grenoble_groupe_local/Agenda#Lundi_12_juin_Atelier_%22Auberge_espagnole%22

Première partie : planification des actions futures
========================================================

- Cartopartie
- Thématique vélo pour la saison prochaine ?

Seconde partie
==================

venez présenter rapidement un sujet OSM que vous avez découvert, sur lequel
vous avez contribué, ou pour lequel vous aimeriez constituer un groupe
de contribution.

Un PC branché sur un vidéoprojecteur sera disponible.

