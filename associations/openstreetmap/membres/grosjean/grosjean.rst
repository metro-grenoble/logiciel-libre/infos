.. index::
   pair: Openstreetmap; Nicolas Grosjean
   ! Nicolas Grosjean

.. _grosjean:

=========================================================================================================================
**Nicolas Grosjean**
=========================================================================================================================

- https://github.com/NicolasGrosjean
- https://wiki.openstreetmap.org/wiki/User:Grosjen
- https://www.openstreetmap.org/user/NicolasGrosjean
