.. index::
   pair: Guilde ; Presentation


.. _guilde_presentation:

========================================================================
Presentation
========================================================================

- https://www.guilde.asso.fr/


La Guilde des Utilisateurs d'Informatique Libre du Dauphiné (GUILDE) est
une association loi 1901 ayant pour objectif de promouvoir les logiciels libres,
et particulièrement le système GNU/Linux, auprès des particuliers et des
professionnels et de rassembler les utilisateurs de la région du Dauphiné.

