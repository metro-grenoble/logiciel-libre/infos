.. index::
   pair: Association; Guilde
   ! Guilde (Guilde des Utilisateurs d'Informatique Libre du Dauphiné)


.. _guilde:

========================================================================
**Guilde** (Guilde des Utilisateurs d'Informatique Libre du Dauphiné)
========================================================================

- https://www.guilde.asso.fr/


.. toctree::
   :maxdepth: 3

   presentation/presentation

